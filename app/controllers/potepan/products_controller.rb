class Potepan::ProductsController < ApplicationController
  # 特定商品の詳細を表示
  def show
    @product = Spree::Product.find(params[:id])
  end
end
